// Playing Cards
// Thay Lee/Luba Letunovskaya

#include <iostream>
#include <conio.h>

using namespace std;

//Rank and suit
enum Rank
{
	Ace = 14,
	King = 13,
	Queen = 12,
	Jack = 11,
	Ten = 10 ,
	Nine = 9,
	Eight = 8,
	Seven = 7,
	Six = 6,
	Five = 5,
	Four = 4,
	Three = 3,
	Two = 2,
};

enum Suit
{
	Spades,
	Hearts,
	Diamonds,
	Clubs
};

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card c)
{
	cout << "The ";

	switch (c.rank)
	{
	case 2:
		cout << "Two of ";
		break;

	case 3:
		cout << "Three of ";
		break;

	case 4:
		cout << "Four of ";
		break;
	case 5:
		cout << "Five of  ";
		break;
	case 6:
		cout << "Six of ";
		break;
	case 7:
		cout << "Seven of  ";
		break;
	case 8:
		cout << "Eight of  ";
		break;
	case 9:
		cout << "Nine of ";
		break;
	case 10:
		cout << "Ten of ";
		break;
	case 11:
		cout << "Jack of ";
		break;
	case 12:
		cout << "Queen of ";
		break;
	case 13:
		cout << "King of ";
		break;
	case 14:
		cout << "Ace of ";
		break;
	}

	switch(c.suit)
	{	
	case Hearts: cout << " hearts.\n";
		break;
	case Clubs: cout << " clubs.\n";
		break;
	case Diamonds: cout << " diamonds.\n";
		break;
	case Spades: cout << " spades.\n";
		break;
	}
	
	
}

Card HighCard(Card card1, Card card2)
{
	if (card1.rank > card2.rank)
	{
		return card1;
	}
	else if (card1.rank < card2.rank)
	{
		return card2;
	}
	else 
		if (card1.suit < card2.suit)
		{
			return card1;
		}
		else if (card2.suit > card1.suit)
		{
			return card2;
		}
	
}

int main()
{
	Card a;
	a.rank = King;
	a.suit = Diamonds;
	

	Card b;
	b.rank = Jack;
	b.suit = Hearts;
	

	Card c;
	c.rank = Ace;
	c.suit = Clubs;
	
	

	PrintCard(HighCard(c, b));
	
	

	
	
	(void)_getch();
	return 0;
}
